# frozen_string_literal: true

# EE:SaaS
module GitlabSubscriptions
  class TrialsController < ApplicationController
    include SafeFormatHelper
    include OneTrustCSP
    include GoogleAnalyticsCSP

    layout 'minimal'

    skip_before_action :set_confirm_warning
    before_action :check_feature_available!
    before_action :authenticate_user!

    feature_category :plan_provisioning
    urgency :low

    def new
      if params[:step] == GitlabSubscriptions::Trials::CreateService::TRIAL
        render :step_namespace
      else
        render :step_lead
      end
    end

    def create
      @result = GitlabSubscriptions::Trials::CreateService.new(
        step: params[:step], lead_params: lead_params, trial_params: trial_params, user: current_user
      ).execute

      if @result.success?
        # lead and trial created
        # Reset on the subscription is needed here since CustomersDot updates the record we are operating
        # on during the request cycle, so it will need updated to see that change as we do not
        # re-find it.
        flash[:success] = success_flash_message(@result.payload[:namespace].gitlab_subscription.reset)

        redirect_to trial_success_path(@result.payload[:namespace])
      elsif @result.reason == GitlabSubscriptions::Trials::CreateService::NO_SINGLE_NAMESPACE
        # lead created, but we now need to select namespace and then apply a trial
        redirect_to new_trial_path(@result.payload[:trial_selection_params])
      elsif @result.reason == GitlabSubscriptions::Trials::CreateService::NOT_FOUND
        # namespace not found/not permitted to create
        render_404
      elsif @result.reason == GitlabSubscriptions::Trials::CreateService::LEAD_FAILED
        render :step_lead_failed
      elsif @result.reason == GitlabSubscriptions::Trials::CreateService::NAMESPACE_CREATE_FAILED
        # namespace creation failed
        params[:namespace_id] = @result.payload[:namespace_id]

        render :step_namespace_failed
      else
        # trial creation failed
        params[:namespace_id] = @result.payload[:namespace_id]

        render :trial_failed
      end
    end

    private

    def trial_success_path(namespace)
      if discover_group_security_flow?
        group_security_dashboard_path(namespace)
      elsif Feature.enabled?(:duo_enterprise_trials, current_user)
        group_settings_gitlab_duo_seat_utilization_index_path(namespace)
      else
        group_path(namespace)
      end
    end

    def authenticate_user!
      return if current_user

      redirect_to(
        new_trial_registration_path(::Onboarding::Status.glm_tracking_params(params)),
        alert: I18n.t('devise.failure.unauthenticated')
      )
    end

    def lead_params
      params.permit(
        *::Onboarding::Status::GLM_PARAMS,
        :company_name, :company_size, :first_name, :last_name, :phone_number,
        :country, :state, :website_url
      ).to_h
    end

    def trial_params
      params.permit(*::Onboarding::Status::GLM_PARAMS, :new_group_name, :namespace_id, :trial_entity)
      .with_defaults(organization_id: Current.organization_id).to_h
    end

    def discover_group_security_flow?
      %w[discover-group-security discover-project-security].include?(params[:glm_content])
    end

    def check_feature_available!
      render_404 unless ::Gitlab::Saas.feature_available?(:subscriptions_trials)
    end

    def success_flash_message(subscription)
      if discover_group_security_flow? || Feature.disabled?(:duo_enterprise_trials, current_user)
        s_("BillingPlans|Congratulations, your free trial is activated.")
      else
        safe_format(
          s_(
            "BillingPlans|You have successfully started an Ultimate and GitLab Duo Enterprise trial that will " \
              "expire on %{exp_date}. " \
              "To give members access to new GitLab Duo Enterprise features, " \
              "%{assign_link_start}assign them%{assign_link_end} to GitLab Duo Enterprise seats."
          ),
          tag_pair(
            helpers.link_to(
              '', help_page_path('subscriptions/subscription-add-ons', anchor: 'assign-gitlab-duo-seats'),
              target: '_blank', rel: 'noopener noreferrer'
            ),
            :assign_link_start, :assign_link_end
          ),
          exp_date: l(subscription.trial_ends_on.to_date, format: :long)
        )
      end
    end
  end
end

GitlabSubscriptions::TrialsController.prepend_mod
